# PHP-QA: Static Analysis Tools for PHP
This [Docker](https://www.docker.com) image provides some static analysis tools for PHP that I use for many of my projects. It is inspired by the wonderful [PHPQA](https://github.com/jakzal/phpqa) [Docker](https://www.docker.com) image made by [Jakub Zalas](https://github.com/jakzal). 

You may want to have a look at that [Docker](https://www.docker.com) image if you need a more complete set of analysis tools.

## Supported platforms and PHP versions
This [Docker](https://www.docker.com) image is based on the PHP 7.4 CLI image

## Available Tools
* composer - [Dependency Manager for PHP](https://getcomposer.org/)
* phpunit - [The PHP testing framework](https://phpunit.de/)
* phan - [Static Analysis Tool](https://github.com/phan/phan)
* phpinsights - [Analyses the code quality of your PHP projects](https://phpinsights.com/)
* security-checker - [Checks if your application uses dependencies with known security vulnerabilities](https://github.com/sensiolabs/security-checker)
* phploc - [A tool for quickly measuring the size of a PHP project](https://github.com/sebastianbergmann/phploc)
* phpmd - [PHP Mess Detector](https://phpmd.org/)
* psalm - [Finds errors in PHP applications](https://getpsalm.org/)
* phpstan - [Static Analysis Tool](https://github.com/phpstan/phpstan)
* phpspec - [SpecBDD Framework for PHP](https://github.com/phpspec/phpspec)
* behat - [BDD in PHP](https://github.com/Behat/Behat)
* php-code-fixer - [Analyzer of PHP code to search issues with deprecations](https://github.com/wapmorgan/PhpCodeFixer)
* http-status-check - [CLI tool to crawl a website and check HTTP status codes ](https://github.com/spatie/http-status-check/)
* deptrac - [Keep your architecture clean](https://github.com/sensiolabs-de/deptrac)

## How to install
At the moment this docker image is not registered to any docker registry yet, so you need to clone this repository:

```bash
git clone https://github.com/stelgenhof/php-qa
```

After cloning the repository, we need to build the docker image:

```bash
docker build -t <your_tag> .
```

## How to use

To run one of the tools inside the container, you will need to mount
the project directory on the container with `-v $(pwd):/project`.

If you want to be able to interrupt the chosen tool when it takes too much time to complete, you can use the `--init` option. Please refer to the [docker run documentation](https://docs.docker.com/engine/reference/commandline/run/) for more information.

Example
```bash
docker run --init -it --rm -v $(pwd):/project -v $(pwd)/tmp-phpqa:/tmp -w /project <your_tag> phpa src
```
For convenience you may want to create an alias for the command:

```bash
alias phpqa='docker run --init -it --rm -v $(pwd):/project -v $(pwd)/tmp-phpqa:/tmp -w /project <your_tag>'
```

Add it to your `~/.bashrc` so it's available every time you start a new terminal session. Now running a tool becomes a lot easier:

```bash
phpqa phpa src
```

Please check the appropriate usage for each of the included tools.
